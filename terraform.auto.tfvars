region = "us-east-1"

vpc_cidr = "10.0.0.0/16"

enable_dns_support = "true"

enable_dns_hostnames = "true"

# enable_classiclink = "false"

# enable_classiclink_dns_support = "false"

preferred_number_of_public_subnets = 2

preferred_number_of_private_subnets = 4

environment = "dev"

ami-web = "ami-0a25f5f9908ffd476"

ami-bastion = "ami-00dd0308751ffa489"

ami-nginx = "ami-06e5a5c59971a8fb7"

ami-sonar = "ami-01434adebf0ca475b"

keypair = "new-keypair-ED25519"

master-password = "devopspblproject"

master-username = "david"

account_no = "159339686012"

tags = {
  Owner-Email     = "infradev-segun@darey.io"
  Managed-By      = "Terraform"
  Billing-Account = "696742900004"
}